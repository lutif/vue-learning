// import axios from 'axios'

const state = {
todos:[
]
}

const getters = {
    allTodos:(state)=>state.todos
}

const actions = {
    async fetchTodos({commit}){
// try {
    
//     const res = await axios.get("https://jsonplaceholder.typicode.com/todos")
//     console.log("data is ",res.data)
// } catch (error) {
// console.log(error)    
// }

const res=[{
    id:1,
    title:"ONE",
    body:"body text"
},
{
    id:2,
    title:"two",
    body:"body text"
},
{
    id:3,
    title:"three",
    body:"body text"
},
{
    id:4,
    title:"four",
    body:"body text"
},]

commit("setTodos",res)
           },
    async addTodo( {commit},todo ){

        if(todo){

            commit("newTodo",todo)
        }
    },
    async removeTodo({commit},id){
        if (id){
            commit ("deleteTodo",id)
        } 
    }
}

const mutations = {
setTodos:(state,todos) => (state.todos= todos),
newTodo:(state, todo)=>(state.todos.unshift({...todo,id:state.todos.length+1})),
deleteTodo:(state,id)=>(state.todos=state.todos.filter(todo=>todo.id!=id))
}

export default {
    state,
    getters,
    actions,
    mutations
};

